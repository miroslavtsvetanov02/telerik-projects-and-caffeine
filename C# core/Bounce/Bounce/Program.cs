﻿using System;

namespace Bounce
{
	internal class Program
	{
		static void Main(string[] args)
		{
            string[] dimensions = Console.ReadLine().Split(' ');
            int totalRows = int.Parse(dimensions[0]);
            int totalCols = int.Parse(dimensions[1]);

            long[,] matrix = new long[totalRows, totalCols];

            for (int row = 0; row < totalRows; row++)
            {
                for (int col = 0; col < totalCols; col++)
                {
                    matrix[row, col] = (long)Math.Pow(2, row + col);
                }
            }

            int currentRow = 0;
            int currentCol = 0;

            int rowDirection = 1; // +1 or -1
            int colDirection = 1; // +1 or -1

            long result = matrix[currentRow, currentCol];

            /*if (totalRows == 1 || totalCols == 1)
            {
                Console.WriteLine(result);
                return;
            }*/

            bool isCornerHit = false;

            while (isCornerHit == false)
            {
                int potentialNextRow = currentRow + rowDirection;
                int potentialNextCol = currentCol + colDirection;

                if (potentialNextRow < 0)
                {
                    // we should go down
                    rowDirection = 1;
                }

                if (potentialNextRow >= totalRows)
                {
                    // we should go up
                    rowDirection = -1;
                }

                if (potentialNextCol < 0)
                {
                    colDirection = 1;
                }

                if (potentialNextCol >= totalCols)
                {
                    colDirection = -1;
                }

                currentRow += rowDirection;
                currentCol += colDirection;

                result += matrix[currentRow, currentCol];

                if ((currentRow == 0 && currentCol == 0)  // top left
                    || (currentRow == 0 && currentCol == totalCols - 1)
                    || (currentCol == 0 && currentRow == totalRows - 1)
                    || (currentCol == totalCols - 1 && currentRow == totalRows - 1))
                {
                    isCornerHit = true;
                }
            }

            Console.WriteLine(result);
        }
	}
}
