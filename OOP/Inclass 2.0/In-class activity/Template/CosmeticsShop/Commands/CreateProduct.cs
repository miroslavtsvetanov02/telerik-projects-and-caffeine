﻿using CosmeticsShop.Core;
using CosmeticsShop.Models;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace CosmeticsShop.Commands
{
    public class CreateProduct : ICommand
    {
        public const int ExpectedNumberOfArguments = 4;
        private readonly CosmeticsRepository cosmeticsRepository;

        public CreateProduct(CosmeticsRepository productRepository)
        {
            this.cosmeticsRepository = productRepository;
        }

        public string Execute(List<string> parameters)
        {
            ValidateHelpers.ValidateArgumentsCount(parameters, ExpectedNumberOfArguments);

            string name = parameters[0];
            string brand = parameters[1];

            double price = ValidateHelpers.ParseDoubleParameter(parameters[2], "price");

            GenderType gender = ValidateHelpers.ParseGenderType(parameters[3]);

            if (cosmeticsRepository.ProductExist(name))
            {
                throw new ArgumentException("Product already exists!");
            }

            this.cosmeticsRepository.CreateProduct(name, brand, price, gender);

            return $"Product with name {name} was created!";
        }
    }
}
