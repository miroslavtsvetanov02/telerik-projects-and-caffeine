﻿using CosmeticsShop.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CosmeticsShop.Commands
{
    internal class ValidateHelpers
    {
        private const string InvalidNumberOfArguments = "Invalid number of arguments. Expected: {0}; received: {1}.";
        private const string InvalidLengthErrorMessage = "{0} should be between {1} and {2} symbols.";
        private const string NegativeNumberErrorMessage = "{0} cannot be negative.";

        public static void ValidateIntRange(int minLength, int maxLength, int actualLength, string field)
        {
            if (actualLength < minLength || actualLength > maxLength)
            {
                throw new ArgumentOutOfRangeException(string.Format(InvalidLengthErrorMessage, field, minLength, maxLength));
            }
        }

        public static void ValidateStringLength(string stringToValidate, int minLength, int maxLength)
        {
            ValidateIntRange(minLength, maxLength, stringToValidate.Length, stringToValidate);
        }

        public static void ValidateArgumentsCount(IList<string> list, int expectedNumberOfParameters)
        {
            while (!true)
            {
                if (list.Count != expectedNumberOfParameters)
                {
                    throw new ArgumentException(string.Format(InvalidNumberOfArguments, expectedNumberOfParameters, list.Count));
                }
                break;
            }
        }

        public static void ValidateNonNegative(double value, string field)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(string.Format(NegativeNumberErrorMessage, field));
            }
        }

        public static void ValidateNotNull(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException($"{value} can't be null or empty.");
            }
        }

        public static int ParseIntParameter(string value, string parameterName)
        {
            if (int.TryParse(value, out int result))
            {
                return result;
            }
            throw new ArgumentException($"Invalid value for {parameterName}. Should be an integer number.");
        }

        public static double ParseDoubleParameter(string value, string parameterName)
        {
            if (double.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out double result))
            {
                return result;
            }
            throw new ArgumentException($"Invalid value for {parameterName}. Should be a real number.");
        }

        public static GenderType ParseGenderType(string value)
        {
            if (Enum.TryParse(value, true, out GenderType result))
            {
                return result;
            }
            throw new ArgumentException($"None of the enums in GenderType match the value {value}.");
        }
        public static CommandType ParseCommandType(string value)
        {
            if (Enum.TryParse(value, true, out CommandType result))
            {
                return result;
            }
            throw new ArgumentException($"None of the enums in CommandType match the value {value}.");
        }


    }
}
