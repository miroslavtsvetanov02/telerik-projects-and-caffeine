﻿using CosmeticsShop.Core;
using System;
using System.Collections.Generic;

namespace CosmeticsShop.Commands
{
    public class CreateCategory : ICommand
    {
        public const int ExpectedNumberOfArguments = 1;

        private readonly CosmeticsRepository cosmeticsRepository;

        public CreateCategory(CosmeticsRepository productRepository)
        {
            this.cosmeticsRepository = productRepository;
        }

        public string Execute(List<string> parameters)
        {
            ValidateHelpers.ValidateArgumentsCount(parameters, ExpectedNumberOfArguments);
            string categoryName = parameters[0];

            if (this.cosmeticsRepository.CategoryExist(categoryName))
            {
                throw new ArgumentException(string.Format($"Category with name {categoryName} already exists!"));
            }

            this.cosmeticsRepository.CreateCategory(categoryName);

            return $"Category with name {categoryName} was created!";
        }

    }
}
