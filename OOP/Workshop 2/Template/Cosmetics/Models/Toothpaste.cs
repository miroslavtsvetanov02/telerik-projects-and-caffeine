﻿using Cosmetics.Helpers;
using Cosmetics.Models.Contracts;
using Cosmetics.Models.Enums;
using System;
using System.Text;

namespace Cosmetics.Models
{
    public class Toothpaste : Product, IToothpaste
    {
        private string ingredients;

        public const int NameMinLength = 3;
        public const int NameMaxLength = 10;
        public const int BrandMinLength = 2;
        public const int BrandMaxLength = 10;

        public Toothpaste(string name, string brand, decimal price, GenderType gender, string ingredients) :
            base(name, brand, price, gender)
		{
            this.ingredients = ingredients;
            ValidationHelper.ValidateIngredients(this.ingredients);
        }
        public string Ingredients
		{
            get
			{
                return this.ingredients;
			}
            set
			{
                this.ingredients = value;
			}
		}

        public override string Print()
		{
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"#{this.Name} {this.Brand}");
            sb.AppendLine($"#Price: ${this.Price}");
            sb.AppendLine($"#Gender: {this.Gender}");
            sb.AppendLine($"#Ingredients: {this.Ingredients}");
            return sb.ToString();
        }
    }
}
