﻿using Cosmetics.Models.Enums;

namespace Cosmetics.Models.Contracts
{
    public interface IProduct
    {
        public string Name { get; }

        public string Brand { get; }

        public decimal Price { get; }

        public GenderType Gender { get; }

        public string Print();
    }
}
