﻿namespace Cosmetics.Models.Contracts
{
    public interface IToothpaste
    {
        public string Ingredients { get; }
    }
}