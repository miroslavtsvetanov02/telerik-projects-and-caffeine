﻿using Cosmetics.Models.Enums;

namespace Cosmetics.Models.Contracts
{
    public interface IShampoo
    {
        public int Millilitres { get; }

        public UsageType Usage { get; }
    }
}