﻿using System;
using System.Collections.Generic;
using System.Text;
using Cosmetics.Models.Enums;
using Cosmetics.Helpers;
using Cosmetics.Models.Contracts;
using System.Linq;

namespace Cosmetics.Models
{
	public abstract class Product : IProduct
	{
        public const int NameMinLength = 3;
        public const int NameMaxLength = 10;
        public const int BrandMinLength = 2;
        public const int BrandMaxLength = 10;

        private string name;
        private string brand;
        private decimal price;
        private GenderType gender;

        public Product(string name, string brand, decimal price, GenderType gender)
        {
            this.Name = name;
            this.Brand = brand;
            this.Price = price;
            this.gender = gender;

            ValidationHelper.ValidateStringLength(name, NameMinLength, NameMaxLength);
            ValidationHelper.ValidateStringLength(brand, BrandMinLength, BrandMaxLength);
            ValidationHelper.ValidateNonNegative(price, "price");
        }

        public decimal Price
        {
            get
            {
                return this.price;
            }
            set
            {
                this.price = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string Brand
        {
            get
            {
                return this.brand;
            }
            set
            {
                this.brand = value;
            }
        }

        public GenderType Gender
        {
            get
            {
                return this.gender;
            }
        }

        public abstract string Print();

        public override bool Equals(object p)
        {
            if (p == null || !(p is Product))
            {
                return false;
            }

            if (this == p)
            {
                return true;
            }

            Product otherProduct = (Product)p;

            return this.Price == otherProduct.Price
                    && this.Name == otherProduct.Name
                    && this.Brand == otherProduct.Brand
                    && this.Gender == otherProduct.Gender;
        }
    }
}
