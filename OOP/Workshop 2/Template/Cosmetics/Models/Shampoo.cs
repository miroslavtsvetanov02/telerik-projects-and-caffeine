﻿using Cosmetics.Helpers;
using Cosmetics.Models.Contracts;
using Cosmetics.Models.Enums;
using System.Collections.Generic;
using System;
using System.Text;

namespace Cosmetics.Models
{
    public class Shampoo : Product, IShampoo
    {
        private int millilitres;
        private UsageType usage;

        public const int NameMinLength = 3;
        public const int NameMaxLength = 10;
        public const int BrandMinLength = 2;
        public const int BrandMaxLength = 10;

        public Shampoo(string name, string brand, decimal price, GenderType gender, int millilitres, UsageType usage) :
            base(name, brand, price, gender)
        {
            this.millilitres = millilitres;
            this.usage = usage;

            ValidationHelper.ValidateNonNegative(millilitres, "mililitres");

            //throw new NotImplementedException("Not implemented yet.");
        }

        public int Millilitres
		{
            get
			{
                return this.millilitres;
			}
            set
			{
                this.millilitres = value;
            }
		}

        public UsageType Usage
		{
            get
			{
                return this.usage;
			}
		}

        public override string Print()
		{
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"#{this.Name} {this.Brand}");
            sb.AppendLine($"#Price: ${this.Price}");
            sb.AppendLine($"#Gender: {this.Gender}");
            sb.AppendLine($"#Milliliters: {this.Millilitres}");
            sb.AppendLine($"#Usage: {this.usage}");
            return sb.ToString();

        }
    }
}
