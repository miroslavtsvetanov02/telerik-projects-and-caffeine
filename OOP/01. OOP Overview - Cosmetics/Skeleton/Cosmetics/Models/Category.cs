﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Cosmetics.Models
{
    public class Category
    {
        public const int NameMinLength = 2;
        public const int NameMaxLength = 15;
        private string categoryName;
        private List<Product> products = new List<Product>();

        public Category(string name)
        {
            this.categoryName = name;
            this.products = new List<Product>();
            

            if (this.categoryName.Length < NameMinLength || this.categoryName.Length > NameMaxLength)
            {
                throw new ArgumentException("Name can't be lower than 2 or higher than 15");
            }
        }

        public string Name
        {
            get
            {
                return categoryName;
            }
            set
            {
                categoryName = value;
            }
            
        }

        public List<Product> Products
        {
            get
            {
                return products;
            }
        }

        public void AddProduct(Product product)
        {
            this.products.Add(product);
            
        }

        public void RemoveProduct(Product product)
        {
            if (!products.Contains(product))
            {
                throw new ArgumentException("product is not found");
            }

            this.products.Remove(product);
        }

        public string Print()
        {
            StringBuilder sb = new StringBuilder();


            sb.AppendLine($"#Category: {this.Name}");
            foreach (var product in products)
            {
                sb.AppendLine($"#{product.Name} {product.Brand}");
                sb.AppendLine($"#Price: ${product.Price}");
                sb.AppendLine($"#Gender {product.Gender}");
                sb.AppendLine($"===");
            }
            if (products.Count == 0)
            {
                sb.AppendLine($"#No products in this category");
            }

            return sb.ToString();

        }
    }
}

