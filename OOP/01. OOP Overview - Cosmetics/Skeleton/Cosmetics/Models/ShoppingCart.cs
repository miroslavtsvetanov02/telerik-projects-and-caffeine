﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Cosmetics.Models
{
    public class ShoppingCart
    {
        private readonly List<Product> products;

        public ShoppingCart()
        {
            this.products = new List<Product>();
        }

        public List<Product> Products
        {
            get
            {
                return products;
            }
        }

        public void AddProduct(Product product)
        {
            this.products.Add(product);
            this.products.OrderBy(p => p.Brand).OrderByDescending(p => p.Price).ToList();
        }

        public void RemoveProduct(Product product)
        {
            this.products.Remove(product);
        }

        public bool ContainsProduct(Product product)
        {
            if (Products.Contains(product))
            {
                return true;
            }
            return false;
        }

        public double TotalPrice()
        {
            double totalPrice = 0;
            foreach (var product in products)
            {
                totalPrice += product.Price;
            }
            return totalPrice;

        }
    }
}
