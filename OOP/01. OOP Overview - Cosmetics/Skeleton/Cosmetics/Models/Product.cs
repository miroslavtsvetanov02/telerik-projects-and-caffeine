﻿using System;
using System.Text;
using System.Linq;

namespace Cosmetics.Models
{
    public class Product
    {
        public const int NameMinLength = 3;
        public const int NameMaxLength = 10;
        public const int BrandMinLength = 2;
        public const int BrandMaxLength = 10;
        private string productName;
        private string brand;
        private double price;
        private GenderType gender;

        public Product(string name, string brand, double price, GenderType gender)
        {
            this.productName = name;
            this.brand = brand;
            this.price = price;
            this.gender = gender;

            if (this.productName.Length < NameMinLength || this.productName.Length > NameMaxLength)
            {
                throw new ArgumentException("Name can't be lower than 2 or higher than 15");
            }

            if (this.brand.Length < BrandMinLength || this.brand.Length > BrandMaxLength)
            {
                throw new ArgumentException("Brand can't be lower than 2 or higher than 15");
            }

            if (this.price <= 0 )
            {
                throw new ArgumentException("Price can't be 0 or lower than 0");
            }

        }

        public double Price
        {
            get
            {
                return this.price;
            }
            set
            {
                this.price = value;
            }
        }

        public string Name
        {
            get
            {
                return this.productName;
            }
            set
            {
                this.productName = value;
            }
        }

        public string Brand
        {
            get
            {
                return this.brand;
            }
            set
            {
                this.brand = value;
            }
        }

        public GenderType Gender
        {
            get
            {
                return this.gender;
            }
        }

        public string Print()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"#{this.Name} {this.Brand}");
            sb.AppendLine($"#Price: ${this.Price}");
            sb.AppendLine($"#Gender: {this.Gender}");
            return sb.ToString();
        }

        public override bool Equals(object p)
        {
            if (p == null || !(p is Product))
            {
                return false;
            }

            if (this == p)
            {
                return true;
            }

            Product otherProduct = (Product)p;

            return this.Price == otherProduct.Price
                    && this.Name == otherProduct.Name
                    && this.Brand == otherProduct.Brand
                    && this.Gender == otherProduct.Gender;
        }
    }
}