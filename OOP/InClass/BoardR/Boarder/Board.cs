﻿using Boarder.Loggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Boarder
{
    internal abstract class Board : ILogger
    {
        private static List<BoardItem> items = new List<BoardItem>();
        

        #region Methods
        public static void AddItem(BoardItem item)
        {
            if (items.Contains(item))
            {
                throw new ArgumentException($"item already exists at {items}");
            }
            items.Add(item);
        }

        public static void LogHistory(ILogger logger)
		{
            items.OrderBy(p => p.DueDate);

            foreach (BoardItem item in items)
            {
                // call the Log() method and give it a string. (the ViewHistory() method returns a string)
                logger.Log(item.ViewHistory());
            }
        }

        public abstract void Log(string value);

        #endregion

        #region Encapsulation
        public static int TotalItems
        {
            get
            {
                return items.Count;
            }
        }
		#endregion
	}
}
