﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;

namespace Boarder
{
	class Issue : BoardItem
	{
		private string description;
        private Status status;

		#region Constructor
		public Issue (string title, string description, DateTime dueDate)
			: base (title, dueDate)
		{
            this.Description = description;

            this.eventList.Clear();
            EventLog createIssueEvent = new EventLog($"Created Issue: '{title}', [{this.status}|{dueDate:dd-MM-yyyy}]. Description: {description}");

            this.eventList.Add(createIssueEvent);
        }
		#endregion

		#region Encapsulation
		public Status Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }
        public string Description
        {
            get 
            { 
                return this.description; 
            }
            set
            {
                if (value == null)
                {
                    value = "No description";
                }
                this.description = value;
            }
        }
        #endregion

        #region Methods
        public override void AdvanceStatus()
        {
            if (this.Status == Status.Verified)
            {
                EventLog notAdvancedEvent = new EventLog($"Issue status already at {this.Status}");
                this.eventList.Add(notAdvancedEvent);

                return;
            }
            else
            {
                EventLog AdvancedEvent = new EventLog($"Issue status set to {Status.Verified}");
                this.eventList.Add(AdvancedEvent);
                this.Status = Status.Verified;

                return;
            }
        }
        public override void RevertStatus()
        {
            if (this.Status == Status.Open)
            {
                EventLog notRevertedEvent = new EventLog($"Issue status already at {this.Status}");
                this.eventList.Add(notRevertedEvent);

                return;
            }

			else
			{
                EventLog RevertedEvent = new EventLog($"Issue status set to {Status.Open}");
                this.eventList.Add(RevertedEvent);
                this.Status = Status.Open;

                return;
            }
        }

        public override string ViewInfo()
        {
            var baseInfo = base.ViewInfo();
            return "Issue: " + baseInfo + $" Description: {Description}";
        }
        #endregion
    }
}
