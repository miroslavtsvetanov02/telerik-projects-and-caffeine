﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Boarder
{
    class Task : BoardItem
    {
        private string assignee;

        #region Constructor
        public Task(string title, string assignee, DateTime dueDate)
            : base(title, dueDate)
        {
            this.Assignee = assignee;
            this.Status = Status.Todo;
            this.eventList.Clear();
            EventLog createTaskEvent = new EventLog($"Created Task: '{title}', [{this.status}|{dueDate:dd-MM-yyyy}]");

            this.eventList.Add(createTaskEvent);
        }
        #endregion

        #region Encapsulation
        public Status Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }

        public string Assignee
        {
            get { return this.assignee; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException();
                }
                if (value.Length < 5 || value.Length > 30)
                {
                    throw new ArgumentException("Lenght cannot be les than 5 or more than 30");
                }
                if (this.assignee != null)
                {
                    EventLog newAssignee = new EventLog($"Assignee changed from {this.assignee} to {value}");
                    this.eventList.Add(newAssignee);
                }
                this.assignee = value;
            }
        }
        #endregion

        #region Methods
        public override void AdvanceStatus()
        {
            if (this.Status == Status.Verified)
            {
                EventLog notAdvancedEvent = new EventLog($"Task status already at Verified");
                this.eventList.Add(notAdvancedEvent);
                return;
            }

            else
            {
                this.status++;
                Status oldStatus = this.Status;
                EventLog advanceEvent = new EventLog($"Task changed from {--oldStatus} to {this.status}");
                this.eventList.Add(advanceEvent);

                return;
            }

        }
        public override void RevertStatus()
        {
            if (this.Status == Status.Todo)
            {
                EventLog notRevertedEvent = new EventLog($"Task status already at Todo");
                this.eventList.Add(notRevertedEvent);

                return;
            }

            else
            {
                this.status--;
                Status oldStatus = this.Status;
                EventLog revertEvent = new EventLog($"Task changed from {++oldStatus} to {this.Status}");
                this.eventList.Add(revertEvent);

                return;
            }
        }

        public override string ViewInfo()
        {
            var baseInfo = base.ViewInfo();
            return "Task: " + baseInfo + $" Assignee: {Assignee}";
        }
        #endregion
    }
}
