﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder.Loggers
{
	internal interface ILogger
	{
		void Log(string value);
	}
}
