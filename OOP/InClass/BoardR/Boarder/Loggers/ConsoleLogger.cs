﻿using Boarder.Loggers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder
{
    internal class ConsoleLogger : ILogger
    {
        public void Log(string value)
        {
            Console.WriteLine(value);
        }

    }
}
