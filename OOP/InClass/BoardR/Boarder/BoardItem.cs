﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Boarder
{
    public abstract class BoardItem
    {
        const string DateFormat = "dd-MM-yyyy";

        protected List<EventLog> eventList = new List<EventLog>();
        protected Status status = Status.Open;
        private string title;
        private string oldTitle = String.Empty;
        private DateTime dueDate;
        private DateTime oldDate;

		#region Constructor
		public BoardItem(string title, DateTime dueDate)
        {

            this.Title = title;
            this.DueDate = dueDate;
            this.oldTitle = title;

            EventLog newTitleEvent = new EventLog($"Item created: '{title}', [{this.status} | {this.dueDate:dd-MM-yyyy}]");
            this.eventList.Add(newTitleEvent);
        }
		#endregion

		#region Encapsulation
		public List<EventLog> EventList
        {
            get
            {
                List<EventLog> copyList = new List<EventLog>(this.eventList); //Encapsulate the list
                return copyList;
            }

        }

        private Status Status
        {
            get
			{
                return this.status;
			}
        }

        public string Title
        {
            get
            {
                return this.title;
            }

            set
            {

                // perform validations
                if (value.Length < 5 || value.Length > 30)
                {
                    throw new ArgumentException("Please provide a name with length between 5 and 30 chars");
                }

                if (oldTitle != String.Empty)
                {
                    EventLog newEvent = new EventLog($"Title changed from '{this.oldTitle}' to '{value}'");
                    this.eventList.Add(newEvent);
                    this.title = value;
                    this.oldTitle = value;
                }

                // set only if validated, otherwise throw ex with some meaningful message
                this.title = value;
            }
        }

        public DateTime DueDate
        {
            get
            {
                return this.dueDate;
            }
            set
            {
                if (value <= DateTime.Now)
                {
                    throw new ArgumentException("DueDate can't be in the past");
                }

                if (this.dueDate != value && this.dueDate != DateTime.MinValue)
                {
                    EventLog newDateEvent = new EventLog($"DueDate changed from '{this.oldDate:dd-MM-yyyy}' to '{value:dd-MM-yyyy}'");
                    this.eventList.Add(newDateEvent);
                }

                this.dueDate = value;
                this.oldDate = value;
            }
        }
        #endregion

        #region Methods
        public abstract void AdvanceStatus();

        public abstract void RevertStatus();
        

        public virtual string ViewInfo()
        {
            return $"'{this.title}', [{this.Status}|{this.dueDate.ToString(DateFormat)}]";
        }

        public string ViewHistory()
        {
            this.eventList.OrderBy(p => p.DateTime);

            StringBuilder sb = new StringBuilder();
            foreach (EventLog eventLog in this.eventList)
            {
                sb.AppendLine(eventLog.ViewInfo());
            }

            return sb.ToString();
        }
		#endregion
	}
}
