﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.VisualBasic;

namespace Boarder
{
	public class EventLog
	{
		const string format = "yyyyMMdd|HH:mm:ss.ffff";

		private string description;
		private DateTime time = DateTime.Now;

		
		public EventLog(string description)
		{
			if (description == null)
			{
				throw new ArgumentNullException();
			}
			this.DateTime = DateTime.Now;
			this.Description = description;
		}
		
		public string Description
		{
			get
			{
				return description;
			}
			private set
			{
				this.description = value;
			}
		}

		public DateTime DateTime
		{
			get
			{
				return time;
			}
			private set
			{
				this.time = value;
			}
		}
	
		public string ViewInfo()
		{
			return $"[{this.time.ToString(format)}]{this.description}";
		}
	}
}
